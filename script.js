// ==UserScript==
// @name         Sncb calendar
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @updateURL    
// @author       You
// @match        https://calendar.google.com/*
// @grant        none
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js
// @require     http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js
// require http://code.jquery.com/jquery-1.12.4.min.js
// ==/UserScript==

(function() {
    'use strict';
    var trains = [
        {title: 'Train', start: '05:56', end: '06:48'},
        {title: 'Train', start: '06:12', end: '07:00'},
        {title: 'Train', start: '06:21', end: '07:15'},
        {title: 'Train', start: '06:40', end: '07:27'},
        {title: 'Train', start: '07:09', end: '08:00'},
        {title: 'Train', start: '07:40', end: '08:28'},
        {title: 'Train', start: '07:56', end: '08:48'},
        {title: 'Train', start: '08:21', end: '09:15'},

        {title: 'Train', start: '16:13', end: '17:04'},
        {title: 'Train', start: '16:27', end: '17:12'},
        {title: 'Train', start: '16:46', end: '17:37'},
        {title: 'Train', start: '16:59', end: '17:47'},
        {title: 'Train', start: '17:12', end: '18:04'},
        {title: 'Train', start: '17:27', end: '18:12'},
        {title: 'Train', start: '17:46', end: '18:37'},
        {title: 'Train', start: '18:12', end: '19:04'},
        {title: 'Train', start: '18:45', end: '19:37'}
    ];
    var pad = function (n){return n<10 ? '0'+n : n;} ;

    function addEvent(start, end, title){
        gapi.client.load('calendar', 'v3', function() {
            var event = {
                'summary' : title,
                'start': {
                    'dateTime': pad(start.getFullYear()) + '-' + pad(start.getMonth()) + '-' + pad(start.getDate()) + 'T' + pad(start.getUTCHours()) + ':' + pad(start.getUTCMinutes()) + ':00-01:00',
                    'timeZone': 'Europe/Brussels'
                },
                'end': {
                    'dateTime': pad(end.getFullYear()) + '-' + pad(end.getMonth()) + '-' + pad(end.getDate()) + 'T' + pad(end.getUTCHours()) + ':' + pad(end.getUTCMinutes()) + ':00-01:00',
                    'timeZone': 'Europe/Brussels'
                }
            };
            var request = gapi.client.calendar.events.insert(
                {
                    'calendarId': '6qah31hur7s0lpg1c8rgllu424@group.calendar.google.com',
                    'resource': event
                }
            ).execute(function(resp){console.log(resp);});
        });
    }

    $(function(){
        var defaultCss = {
            border: 'solid 1px black',
            padding: '5px',
            'background-color': "white",
            display: 'inline-block'
        };
        /*
        var $snapTo = $('.tg-col-eventwrapper');
        var $dragCss = {
            border: 'solid 1px red',
            display: 'block',
            width: $snapTo.width()
        };
        */
        var links = [];
        $.each(trains, function(id, data){
            links.push($('<a>').css(defaultCss).addClass('train').text(data.title + '(' + data.start + ' - ' + data.end + ')').attr('data-start', data.start).attr('data-end', data.end).attr('data-title', data.title));
        });
        $(document).on('click', '.train', function(){
            console.log($(this));
            var dataStart = $(this).attr('data-start');
            var dataEnd = $(this).attr('data-end');
            var dataTitle = $(this).attr('data-title');
            var start = new Date(2017, 3, 10);
            start.setHours(dataStart.split(':')[0]);
            start.setMinutes(dataStart.split(':')[1]);
            var end = new Date(2017, 3, 10);
            end.setHours(dataEnd.split(':')[0]);
            end.setMinutes(dataEnd.split(':')[1]);
            addEvent(start, end, dataTitle);
        });
        /*
        $link.draggable({
            stack: '#gridcontainer',
            snap: $snapTo,
            start: function(){
                $link.css($dragCss);
            },
            stop: function(){
                $link.css($defaultCss);
            }
        });
        */
        $('#gridcontainer').prepend(links);
    });
    // Your code here...
})();


